import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_single_input_field(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
        wait = WebDriverWait(driver, 10)
        button_close = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="at-cv-lightbox-close"]')))
        message_input = driver.find_element_by_id("user-message")
        show_button = driver.find_element_by_xpath("//button[contains(text(),'Show Message')]")
        text_output = driver.find_element_by_xpath("//*[@id='display']")

        button_close.click()
        message_input.send_keys("Hola")
        show_button.click()
        self.assertEqual("Hola", text_output.text)

    def test_single_checkbox_demo(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html")
        check_box_button = driver.find_element_by_xpath("//input[@type='checkbox' and @id='isAgeSelected']")
        text_checkbox = driver.find_element_by_xpath("//div[@id='txtAge']")

        check_box_button.click()
        self.assertEqual("Success - Check box is checked", text_checkbox.text)

    def test_radio_button_demo(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html")
        radio_button_male = driver.find_element_by_xpath("//input[@value='Male' and @name='optradio']")
        button_checked_value = driver.find_element_by_xpath("//button[@id='buttoncheck']")
        text_radio_button = driver.find_element_by_xpath("//p[@class='radiobutton']")

        radio_button_male.click()
        button_checked_value.click()
        self.assertRegex(text_radio_button.text, "Male")


if __name__ == '__main__':
    unittest.main()